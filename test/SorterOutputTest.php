<?php

namespace load\test;

use load\app\Constants;
use load\app\SorterOutput;
use load\app\sorters\SorterFactory;
use PHPUnit\Framework\TestCase;

class SorterOutputTest extends TestCase
{
    protected $array = [
        [1, 1, 1, 1, 1],
        [2, 2, 2, 2, 2],
        [3, 3, 3, 3, 3],
        [4, 4, 4, 4, 4],
        [5, 5, 5, 5, 5]
    ];
    protected $output;
    protected $factory;

    protected function setUp()
    {
        $this->output = new SorterOutput();
        $this->factory = SorterFactory::getInstance();
    }

    protected function tearDown()
    {
        $this->output = null;
        $this->factory = null;
    }

    public function testSorterBrowserOutput()
    {
        $diagonal = $this->factory->getSorter(Constants::DIAGONAL_NAME, $this->array);
        $output = $this->output;
        $this->expectOutputString('Diagonal in:<br/>&nbsp;&nbsp;1&nbsp;&nbsp;1&nbsp;&nbsp;1&nbsp;&nbsp;1&nbsp;&nbsp;1<br/>&nbsp;&nbsp;2&nbsp;&nbsp;2&nbsp;&nbsp;2&nbsp;&nbsp;2&nbsp;&nbsp;2<br/>&nbsp;&nbsp;3&nbsp;&nbsp;3&nbsp;&nbsp;3&nbsp;&nbsp;3&nbsp;&nbsp;3<br/>&nbsp;&nbsp;4&nbsp;&nbsp;4&nbsp;&nbsp;4&nbsp;&nbsp;4&nbsp;&nbsp;4<br/>&nbsp;&nbsp;5&nbsp;&nbsp;5&nbsp;&nbsp;5&nbsp;&nbsp;5&nbsp;&nbsp;5<br/><br/>Diagonal out:<br/>&nbsp;&nbsp;1&nbsp;&nbsp;1&nbsp;&nbsp;2&nbsp;&nbsp;2&nbsp;&nbsp;3<br/>&nbsp;&nbsp;1&nbsp;&nbsp;1&nbsp;&nbsp;2&nbsp;&nbsp;3&nbsp;&nbsp;4<br/>&nbsp;&nbsp;1&nbsp;&nbsp;2&nbsp;&nbsp;3&nbsp;&nbsp;4&nbsp;&nbsp;5<br/>&nbsp;&nbsp;2&nbsp;&nbsp;3&nbsp;&nbsp;4&nbsp;&nbsp;5&nbsp;&nbsp;5<br/>&nbsp;&nbsp;3&nbsp;&nbsp;4&nbsp;&nbsp;4&nbsp;&nbsp;5&nbsp;&nbsp;5<br/>--------------------<br/>');
        $output($diagonal, 'b');
    }

    public function testSorterCmdOutput()
    {
        $diagonal = $this->factory->getSorter(Constants::DIAGONAL_NAME, $this->array);
        $output = $this->output;
        $this->expectOutputRegex("*Diagonal\sout\:*");
        $output($diagonal, 'c');
    }

    public function testOutputDirectoryExists()
    {
        $this->assertDirectoryExists('storage');
    }

    public function testModeExceptionBC()
    {
        $diagonal = $this->factory->getSorter(Constants::DIAGONAL_NAME, $this->array);
        $output = $this->output;
        $this->expectOutputRegex("*Trying\sto\suse\sboth\s\'b\'\s\(Browser\)\sand\s\'c\'\(CLI\)*");
        $output($diagonal, 'bc');
    }

    public function testModeExceptionWithNone()
    {
        $diagonal = $this->factory->getSorter(Constants::DIAGONAL_NAME, $this->array);
        $output = $this->output;
        $this->expectOutputRegex("*Undefined\sflags\.\sUse\s\'c\'\s-\sCLI\sor\s\'b\'\s-\sBrowser\sand/or\s\'f\'*");
        $output($diagonal, 'qwerty');
    }

}
