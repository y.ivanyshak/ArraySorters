<?php

namespace load\test\output;

use load\app\sorters\DiagonalSorter;
use PHPUnit\Framework\TestCase;

class FileOutputTest extends TestCase
{
    public function testPrintArrayToFile()
    {
        $mock= $this->getMockBuilder(DiagonalSorter::class)
            ->disableOriginalConstructor()
            ->setMethods(['printArrayToFile'])
            ->getMock();

        $mock->expects($this->once())
            ->method('printArrayToFile')
            ->willReturn(true);

        $this->assertTrue($mock->printArrayToFile([[1,1,1], [2,2,2]]));
    }
}
