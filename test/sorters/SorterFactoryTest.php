<?php

namespace load\test\sorters;

use load\app\Constants;
use load\app\sorters\DiagonalSorter;
use load\app\sorters\HorisontalSorter;
use load\app\sorters\HorisontalVerticalSorter;
use load\app\sorters\SnailSorter;
use load\app\sorters\SnakeSorter;
use load\app\sorters\SorterFactory;
use load\app\sorters\VerticalSorter;
use PHPUnit\Framework\TestCase;

class SorterFactoryTest extends TestCase
{
    protected $array = [
        [1, 1, 1, 1, 1],
        [2, 2, 2, 2, 2],
        [3, 3, 3, 3, 3],
        [4, 4, 4, 4, 4],
        [5, 5, 5, 5, 5]
    ];

    protected $factory;

    protected function setUp()
    {
        $this->factory = SorterFactory::getInstance();
    }

    protected function tearDown()
    {
        $this->factory = null;
    }

    /**
     * @dataProvider sorterDataProvider
     */
    public function testGetSorter($inputedName, $expected)
    {
        $object = $this->factory->getSorter($inputedName, $this->array);

        $this->assertInstanceOf($expected, $object);
    }

    public function testGetSorterWrongName()
    {
        $this->expectOutputRegex("*The\s<b>Wrong name</b>\sis out of stock\.\sPls\,\schoose\sanother\ssorter\!*");
        $this->factory->getSorter('Wrong name', $this->array);
    }

    public function testGetSorterWithoutArray()
    {
        $this->expectOutputRegex("*Inputed\svalue\sisn\'t\sarray\sand\scan\'t\sbe\ssorted*");
        $this->factory->getSorter(Constants::DIAGONAL_NAME, 'Not an array');
    }

    public function sorterDataProvider()
    {
        return [
            [Constants::HORISONTAL_NAME, HorisontalSorter::class],
            [Constants::VERTICAL_NAME, VerticalSorter::class],
            [Constants::SNAKE_NAME, SnakeSorter::class],
            [Constants::DIAGONAL_NAME, DiagonalSorter::class],
            [Constants::HORISONTAL_VERTICAL_NAME, HorisontalVerticalSorter::class],
            [Constants::SNAIL_NAME, SnailSorter::class]
        ];
    }
}
