<?php

namespace load\test\sorters;

use PHPUnit\Framework\TestCase;

abstract class AbstractSorterTest extends TestCase
{
    protected $sorter;

    protected function tearDown()
    {
        $this->sorter = null;
    }

    /**
     * @dataProvider arrayDataProvider
     */
    abstract protected function testSort($initial, $expected);

    abstract protected function testGetName();

    abstract protected function arrayDataProvider();
}
