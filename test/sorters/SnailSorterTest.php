<?php

namespace load\app\test\sorters;

use load\app\Constants;
use load\test\sorters\AbstractSorterTest;
use load\app\sorters\SnailSorter;

class SnailSorterTest extends AbstractSorterTest
{
    /**
     * @dataProvider arrayDataProvider
     */
    public function testSort($initial, $expected)
    {
        $this->sorter = new SnailSorter($initial);
        $sorted = $this->sorter->sort();
        $this->assertEquals($expected, $sorted);        # Failure if an array isn't sorted
    }

    public function testGetName()
    {
        $this->sorter = new SnailSorter([[1, 1, 1], [2, 2, 2]]);
        $name = $this->sorter->getName();
        $this->assertEquals(Constants::SNAIL_NAME, $name);
    }

    public function arrayDataProvider()
    {
        return [
            [
                [
                    [1, 2, 3, 4, 5],
                    [6, 7, 8, 9, 10],
                    [11, 12, 13, 14, 15],
                    [16, 17, 18, 19, 20],
                    [21, 22, 23, 24, 25]
                ],

                [
                    [1, 2, 3, 4, 5],
                    [16, 17, 18, 19, 6],
                    [15, 24, 25, 20, 7],
                    [14, 23, 22, 21, 8],
                    [13, 12, 11, 10, 9]
                ]
            ],

            [
                [
                    [1, 2, 3],
                    [4, 5, 6],
                    [7, 8, 9]
                ],
                [
                    [1, 2, 3],
                    [8, 9, 4],
                    [7, 6, 5]
                ]
            ],

            [
                [
                    [1, 2, 3],
                    [4, 5, 6],
                    [7, 8, 9],
                    [10, 11, 12],
                    [13, 14, 15]
                ],
                [
                    [1, 2, 3],
                    [12, 13, 4],
                    [11, 14, 5],
                    [10, 15, 6],
                    [9, 8, 7]
                ]
            ],
        ];
    }
}
