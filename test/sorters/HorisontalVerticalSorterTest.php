<?php

namespace load\app\test\sorters;

use load\app\Constants;
use load\test\sorters\AbstractSorterTest;
use load\app\sorters\HorisontalVerticalSorter;

class HorisontalVerticalSorterTest extends AbstractSorterTest
{
    /**
     * @dataProvider arrayDataProvider
     */
    public function testSort($initial, $expected)
    {
        $this->sorter = new HorisontalVerticalSorter($initial);
        $sorted = $this->sorter->sort();
        $this->assertEquals($expected, $sorted);        # Failure if an array isn't sorted
    }

    public function testGetName()
    {
        $this->sorter = new HorisontalVerticalSorter([[1, 1, 1], [2, 2, 2]]);
        $name = $this->sorter->getName();
        $this->assertEquals(Constants::HORISONTAL_VERTICAL_NAME, $name);
    }

    public function arrayDataProvider()
    {
        return [
            [
                [
                    [1, 2, 3, 4, 5],
                    [6, 7, 8, 9, 10],
                    [11, 12, 13, 14, 15],
                    [16, 17, 18, 19, 20],
                    [21, 22, 23, 24, 25]
                ],
                [
                    [1, 2, 3, 4, 5],
                    [20, 6, 7, 8, 25],
                    [19, 21, 9, 22, 24],
                    [18, 10, 11, 12, 23],
                    [13, 14, 15, 16, 17]
                ]
            ]
        ];
    }
}
