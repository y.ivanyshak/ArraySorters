<?php

namespace load\test\sorters;

use load\app\sorters\SorterTools;
use PHPUnit\Framework\TestCase;

class SorterToolsTest extends TestCase
{
    /**
     * @dataProvider arrayDataProvider
     */
    public function testArrayMerge($initial)
    {
        $merged = SorterTools::mergeArray($initial);
        $this->assertTrue(count($merged) == count($initial, COUNT_RECURSIVE)); # Failure if array isn't merged
    }

    /**
     * @dataProvider arrayDataProvider
     */
    public function testArrayType($initial)
    {
        $merged = SorterTools::mergeArray($initial);
        $this->assertInternalType('array', $merged);   # Failure if sorter return not an array
    }

    public function arrayDataProvider()
    {
        return [
            [
                [1, 2, 3, 4, 5],
                [6, 7, 8, 9, 10],
                [11, 12, 13, 14, 15],
                [16, 17, 18, 19, 20],
                [21, 22, 23, 24, 25]
            ],

            [
                [1, 1, 1, 1],
                [2, 2, 2, 2],
                [3, 3, 3, 3],
                [4, 4, 4, 4]
            ],

            [
                [4, 3, 5],
                [4, 3, 5],
                [4, 3, 5],
                [2, 1, 2],
                [1, 2, 1],
                [9, 8, 6]
            ],

            [
                [1, 1],
                [2, 2, 2, 2, 2],
                [3, 3, 3, 3, 3, 3, 3],
                [4, 4],
                [5, 5, 5]
            ]
        ];
    }
}
