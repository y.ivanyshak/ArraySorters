<?php

namespace load\app\test\sorters;

use load\app\Constants;
use load\test\sorters\AbstractSorterTest;
use load\app\sorters\DiagonalSorter;

class DiagonalSorterTest extends AbstractSorterTest
{

    /**
     * @dataProvider arrayDataProvider
     */
    public function testSort($initial, $expected)
    {
        $this->sorter = new DiagonalSorter($initial);
        $sorted = $this->sorter->sort();
        $this->assertEquals($expected, $sorted);        # Failure if an array isn't sorted
    }

    public function testGetName()
    {
        $this->sorter = new DiagonalSorter([[1, 1, 1], [2, 2, 2]]);
        $name = $this->sorter->getName();
        $this->assertEquals(Constants::DIAGONAL_NAME, $name);
    }

    public function arrayDataProvider()
    {
        return [
            [
                [
                    [1, 1, 1, 1, 1],
                    [2, 2, 2, 2, 2],
                    [3, 3, 3, 3, 3],
                    [4, 4, 4, 4, 4],
                    [5, 5, 5, 5, 5]
                ],
                [
                    [1, 1, 2, 2, 3],
                    [1, 1, 2, 3, 4],
                    [1, 2, 3, 4, 5],
                    [2, 3, 4, 5, 5],
                    [3, 4, 4, 5, 5]
                ]
            ],

            [
                [
                    [1, 2, 3, 4, 5],
                    [6, 7, 8, 9, 10],
                    [11, 12, 13, 14, 15],
                    [16, 17, 18, 19, 20],
                    [21, 22, 23, 24, 25]
                ],
                [
                    [1, 3, 6, 10, 15],
                    [2, 5, 9, 14, 19],
                    [4, 8, 13, 18, 22],
                    [7, 12, 17, 21, 24],
                    [11, 16, 20, 23, 25]
                ],
            ],

            [
                [
                    [1, 1, 1, 1],
                    [2, 2, 2, 2],
                    [3, 3, 3, 3],
                    [4, 4, 4, 4]
                ],
                [
                    [1, 1, 2, 3],
                    [1, 2, 3, 4],
                    [1, 2, 3, 4],
                    [2, 3, 4, 4],
                ]
            ]
        ];
    }
}
