<?php

namespace load\app\test\sorters;

use load\app\Constants;
use load\test\sorters\AbstractSorterTest;
use load\app\sorters\HorisontalSorter;

class HorisontalSorterTest extends AbstractSorterTest
{
    /**
     * @dataProvider arrayDataProvider
     */
    public function testSort($initial, $expected)
    {
        $this->sorter = new HorisontalSorter($initial);
        $sorted = $this->sorter->sort();
        $this->assertEquals($expected, $sorted);        # Failure if an array isn't sorted
    }

    public function testGetName()
    {
        $this->sorter = new HorisontalSorter([[1, 1, 1], [2, 2, 2]]);
        $name = $this->sorter->getName();
        $this->assertEquals(Constants::HORISONTAL_NAME, $name);
    }

    public function arrayDataProvider()
    {
        return [
            [
                [
                    [4, 3, 5, 6, 7],
                    [4, 3, 5, 6, 7],
                    [4, 3, 5, 6, 7],
                    [4, 3, 5, 6, 7],
                    [4, 3, 5, 6, 7]
                ],
                [
                    [3, 3, 3, 3, 3],
                    [4, 4, 4, 4, 4],
                    [5, 5, 5, 5, 5],
                    [6, 6, 6, 6, 6],
                    [7, 7, 7, 7, 7]
                ]
            ],

            [
                [
                    [1, 10, 100, 1000, -1],
                    [-2, 2, 20, 200, 2000],
                    [3000, 300, 30, 3, -3],
                    [0, 0, 0, 0, 0],
                    [-4, -5, 4, 5, 25]
                ],
                [
                    [-5, -4, -3, -2, -1],
                    [0, 0, 0, 0, 0],
                    [1, 2, 3, 4, 5],
                    [10, 20, 25, 30, 100],
                    [200, 300, 1000, 2000, 3000]
                ]
            ],

            [
                [
                    [4, 3, 5],
                    [4, 3, 5],
                    [4, 3, 5],
                    [2, 1, 2],
                    [1, 2, 1],
                    [9, 8, 6]
                ],
                [
                    [1, 1, 1],
                    [2, 2, 2],
                    [3, 3, 3],
                    [4, 4, 4],
                    [5, 5, 5],
                    [6, 8, 9]
                ]
            ]
        ];
    }
}
