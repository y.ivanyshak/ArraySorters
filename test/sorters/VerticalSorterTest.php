<?php

namespace load\app\test\sorters;

use load\app\Constants;
use load\test\sorters\AbstractSorterTest;
use load\app\sorters\VerticalSorter;

class VerticalSorterTest extends AbstractSorterTest
{
    /**
     * @dataProvider arrayDataProvider
     */
    public function testSort($initial, $expected)
    {
        $this->sorter = new VerticalSorter($initial);
        $sorted = $this->sorter->sort();
        $this->assertEquals($expected, $sorted);        # Failure if an array isn't sorted
    }

    public function testGetName()
    {
        $this->sorter = new VerticalSorter([[1, 1, 1], [2, 2, 2]]);
        $name = $this->sorter->getName();
        $this->assertEquals(Constants::VERTICAL_NAME, $name);
    }

    public function arrayDataProvider()
    {
        return [
            [
                [
                    [1, 1, 1, 1, 1],
                    [2, 2, 2, 2, 2],
                    [3, 3, 3, 3, 3],
                    [4, 4, 4, 4, 4],
                    [5, 5, 5, 5, 5]
                ],

                [
                    [1, 2, 3, 4, 5],
                    [1, 2, 3, 4, 5],
                    [1, 2, 3, 4, 5],
                    [1, 2, 3, 4, 5],
                    [1, 2, 3, 4, 5]
                ]
            ],

            [
                [
                    [1, 1, 1, 1, 1],
                    [2, 2, 2, 2, 2],
                    [3, 3, 3, 3, 3]
                ],

                [
                    [1, 2, 3],
                    [1, 2, 3],
                    [1, 2, 3],
                    [1, 2, 3],
                    [1, 2, 3]
                ]
            ]
        ];
    }
}
