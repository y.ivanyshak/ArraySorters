<?php

namespace load\app\test\sorters;

use load\app\Constants;
use load\test\sorters\AbstractSorterTest;
use load\app\sorters\SnakeSorter;

class SnakeSorterTest extends AbstractSorterTest
{
    /**
     * @dataProvider arrayDataProvider
     */
    public function testSort($initial, $expected)
    {
        $this->sorter = new SnakeSorter($initial);
        $sorted = $this->sorter->sort();

        $this->assertInternalType('array', $sorted);   # Failure if sorter return not an array
        $this->assertTrue(count($sorted, COUNT_RECURSIVE) != count($initial)); #Failure if sorter return not a two-dimensional array
        $this->assertEquals($expected, $sorted);        # Failure if an array isn't sorted
    }

    public function testGetName()
    {
        $this->sorter = new SnakeSorter([[1, 1, 1], [2, 2, 2]]);
        $name = $this->sorter->getName();
        $this->assertEquals(Constants::SNAKE_NAME, $name);
    }

    public function arrayDataProvider()
    {
        return [
            [
                [
                    [1, 2, 3, 4, 5],
                    [6, 7, 8, 9, 10],
                    [11, 12, 13, 14, 15],
                    [16, 17, 18, 19, 20],
                    [21, 22, 23, 24, 25]
                ],

                [
                    [1, 2, 3, 4, 5],
                    [10, 9, 8, 7, 6],
                    [11, 12, 13, 14, 15],
                    [20, 19, 18, 17, 16],
                    [21, 22, 23, 24, 25]
                ]
            ],

            [
                [
                    [1, 2],
                    [3, 4],
                    [5, 6],
                    [7, 8],
                    [9, 10]
                ],

                [
                    [1, 2],
                    [4, 3],
                    [5, 6],
                    [8, 7],
                    [9, 10]
                ]
            ]
        ];
    }
}
