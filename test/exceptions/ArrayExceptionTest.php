<?php
/**
 * Created by PhpStorm.
 * User: Юра
 * Date: 10.02.2017
 * Time: 17:48
 */

namespace load\test\exceptions;

use load\app\exceptions\ArrayException;

class ArrayExceptionTest extends AbstractExceptionTest
{
    public function testException()
    {
        try {
            throw new ArrayException($this->message, $this->code);
        }
        catch (ArrayException $arrayException) {
            $this->assertEquals($this->message, $arrayException->getMessage());
            $this->assertEquals($this->code, $arrayException->getCode());
            return;
        }

        $this->fail('An expected exception has not been raised.');
    }

    public function testShowException()
    {
        try {
            throw new ArrayException($this->message, $this->code);
        }
        catch (ArrayException $arrayException) {
            $code = $arrayException->getCode();
            $msg = $arrayException->getMessage();
            $filepath = $arrayException->getFile();
            $line = $arrayException->getLine();
            $this->expectOutputString("Exception - $code: '$msg', thrown in: <b>$filepath</b> on line - <b>$line</b><br/>");
            $arrayException->showException();
            return;
        }

        $this->fail('An expected exception has not been raised.');
    }
}
