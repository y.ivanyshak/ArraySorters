<?php

namespace load\test\exceptions;

use load\app\exceptions\ModeException;

class ModeExceptionTest extends AbstractExceptionTest
{
    public function testException()
    {
        try {
            throw new ModeException($this->message, $this->code);
        }
        catch (ModeException $modeException) {
            $this->assertEquals($this->message, $modeException->getMessage());
            $this->assertEquals($this->code, $modeException->getCode());
            return;
        }

        $this->fail('An expected exception has not been raised.');
    }

    public function testShowException()
    {
        try {
            throw new ModeException($this->message, $this->code);
        }
        catch (ModeException $modeException) {
            $code = $modeException->getCode();
            $msg = $modeException->getMessage();
            $filepath = $modeException->getFile();
            $line = $modeException->getLine();
            $this->expectOutputString("Exception - $code: '$msg', thrown in: <b>$filepath</b> on line - <b>$line</b><br/>");
            $modeException->showException();
            return;
        }

        $this->fail('An expected exception has not been raised.');
    }
}
