<?php
/**
 * Created by PhpStorm.
 * User: Юра
 * Date: 10.02.2017
 * Time: 17:49
 */

namespace load\test\exceptions;


use load\app\exceptions\FactoryException;

class FactoryExceptionTest extends AbstractExceptionTest
{
    public function testException()
    {
        try {
            throw new FactoryException($this->message, $this->code);
        }
        catch (FactoryException $factoryException) {
            $this->assertEquals($this->message, $factoryException->getMessage());
            $this->assertEquals($this->code, $factoryException->getCode());
            return;
        }

        $this->fail('An expected exception has not been raised.');
    }

    public function testShowException()
    {
        try {
            throw new FactoryException($this->message, $this->code);
        }
        catch (FactoryException $factoryException) {
            $code = $factoryException->getCode();
            $msg = $factoryException->getMessage();
            $filepath = $factoryException->getFile();
            $line = $factoryException->getLine();
            $this->expectOutputString("Exception - $code: '$msg', thrown in: <b>$filepath</b> on line - <b>$line</b><br/>");
            $factoryException->showException();
            return;
        }

        $this->fail('An expected exception has not been raised.');
    }
}
