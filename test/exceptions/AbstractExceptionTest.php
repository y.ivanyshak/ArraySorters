<?php
/**
 * Created by PhpStorm.
 * User: Юра
 * Date: 10.02.2017
 * Time: 17:48
 */

namespace load\test\exceptions;


use PHPUnit\Framework\TestCase;

abstract class AbstractExceptionTest extends TestCase
{
    protected $message = 'Test Exception';
    protected $code = 101;

    abstract public function testException();

    abstract public function testShowException();
}
