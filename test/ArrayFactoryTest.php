<?php

namespace load\test;

use load\app\ArrayFactory;
use PHPUnit\Framework\TestCase;

class ArrayFactoryTest extends TestCase
{
    protected $array;

    protected function setUp()
    {
        $this->factory = ArrayFactory::getInstance();
    }

    protected function tearDown()
    {
        $this->factory = null;
    }

    /**
     * @dataProvider sorterDataProvider
     */
    public function testGetSorter($inputedNumber, $expected)
    {
        $this->array = ArrayFactory::getArray($inputedNumber);

        $this->assertEquals($expected, $this->array);
    }

    public function testGetSorterWithWrongNumber()
    {
        $num = 'Wrong number';
        $this->array = ArrayFactory::getArray($num);
        $this->expectOutputRegex("*The\s<b>case\s\-\s$num</b>\sis\sout\sof\sstock\.\sPls\,\schoose\sanother\scase\snumber!*");
    }

    public function sorterDataProvider ()
    {
        return [
            [1, [
                [4, 3, 5, 6, 7],
                [4, 3, 5, 6, 7],
                [4, 3, 5, 6, 7],
                [4, 3, 5, 6, 7],
                [4, 3, 5, 6, 7]
            ]],

            [2, [
                [1, 1, 1, 1, 1],
                [2, 2, 2, 2, 2],
                [3, 3, 3, 3, 3],
                [4, 4, 4, 4, 4],
                [5, 5, 5, 5, 5]
            ]],
            [3, [
                [1, 2, 3, 4, 5],
                [6, 7, 8, 9, 10],
                [11, 12, 13, 14, 15],
                [16, 17, 18, 19, 20],
                [21, 22, 23, 24, 25]
            ]],
        ];
    }
}
