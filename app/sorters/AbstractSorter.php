<?php

namespace load\app\sorters;

use load\app\exceptions\ArrayException;
use load\app\outputs\BrowserCmdOutput;
use load\app\outputs\FileOutput;

abstract class AbstractSorter
{
    use BrowserCmdOutput, FileOutput;

    protected $name;

    public function __construct($array)
    {
        try {
            if (!is_array($array)) {
                throw new ArrayException("Inputed value isn't array and can't be sorted");
            }
            $this->array = $array;
            $this->rows = count($this->array);
            $this->columns = count($this->array[0]);
        }
        catch (ArrayException $arrayException) {
            $arrayException->showException();
        }
    }

    abstract public function sort();

    final public function getName()
    {
        return $this->name;
    }
}
