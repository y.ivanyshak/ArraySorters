<?php

namespace load\app\sorters;

final class SorterTools
{
    final public static function mergeArray($array)
    {
        $mergedarray = [];
        $count = 0;

        foreach ($array as $row) {
            $columns = count($row);
            for ($i = 0; $i < $columns; $i++) {
                $mergedarray[$count] = $row[$i];
                $count++;
            }
        }

        return $mergedarray;
    }
}
