<?php

namespace load\app\sorters;

use load\app\Constants;

class DiagonalSorter extends AbstractSorter
{
    protected $name = Constants::DIAGONAL_NAME;

    final public function sort()
    {
        $sortedarray = [];

        $x = 0;
        $y = 0;
        $startforx = 0;
        $startfory = 0;
        $endforx = 0;
        $endfory = $this->columns - 1;

        for ($i = 0; $i < $this->rows; $i++) {
            for ($j = 0; $j < $this->columns; $j++) {
                $sortedarray[$x][$y] = $this->array[$i][$j];

                if ($x != $endforx) {
                    $x--;
                    $y++;
                } elseif ($x == $endforx and $y == $endfory) {
                    $endforx++;
                    $startfory++;
                    $y = $startfory;
                    $x = $startforx;
                } elseif ($x == $endforx and $y != $endfory) {
                    $startforx++;
                    $x = $startforx;
                    $y = $startfory;
                }
            }
        }

        return $sortedarray;
    }
}
