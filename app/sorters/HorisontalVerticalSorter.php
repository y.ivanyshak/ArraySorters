<?php

namespace load\app\sorters;

use load\app\Constants;

final class HorisontalVerticalSorter extends AbstractSorter
{
    protected $name = Constants::HORISONTAL_VERTICAL_NAME;

    final public function sort()
    {
        $sortedarray = [];
        $leftright = true;
        $downup = false;

        $x = 0;
        $y = 0;
        $startforx = 0;
        $startfory = 0;
        $rowend = $this->rows - 1;
        $columnend = $this->columns-1;

        for ($i = 0; $i < $this->rows; $i++) {
            for ($j = 0; $j < $this->columns; $j++) {
                $sortedarray[$x][$y] = $this->array[$i][$j];

                if ($leftright) {
                    if ($x < intdiv($this->rows, 2)) {
                        if ($y == $columnend) {
                            $columnend--;
                            $startforx++;
                            $x = $startforx;
                            $startfory++;
                            $y = $startforx;
                        } else {
                            $y++;
                        }
                    } elseif ($x >= intdiv($this->rows, 2) and $x <= $rowend) {
                        if ($y == $columnend and $x == $rowend) {
                            $leftright = false;
                            $downup = true;
                            $rowend = 1;
                            $startforx = count($this->array) - 1;
                            $x = $startforx;
                            $startfory = 0;
                            $y = $startfory;
                        } elseif ($y == $columnend) {
                            $columnend++;
                            $startforx++;
                            $x = $startforx;
                            $startfory--;
                            $y = $startfory;
                        } else {
                            $y++;
                        }
                    }
                }

                if ($downup) {
                    if ($y < intdiv($this->columns, 2) - 1 ) {
                        if ($x == $rowend) {
                            $rowend++;
                            $startfory++;
                            $y = $startfory;
                            $startforx-=2;
                            $x = $startforx;
                        } else {
                            $x--;
                        }
                    } elseif ($y == intdiv($this->columns, 2) - 1) {
                        while ($y <= intdiv($this->columns, 2)) {
                            $startfory++;
                            $y = $startfory;
                        }
                    } elseif ($y > intdiv($this->columns, 2) and $y <= $columnend) {
                        if ($x == $rowend) {
                            $rowend--;
                            $startfory++;
                            $y = $startfory;
                            $startforx++;
                            $x = $startforx;
                        } else {
                            $x--;
                        }
                    }
                }
            }
        }

        return $sortedarray;
    }
}
