<?php

namespace load\app\sorters;

use load\app\Constants;
use load\app\exceptions\FactoryException;
use load\app\patterns\SingletonTrait;

final class SorterFactory
{
    use SingletonTrait;

    final public function getSorter($sorter, $array)
    {
        try {
            switch ($sorter) {
                case Constants::HORISONTAL_NAME:
                    return new HorisontalSorter($array);
                    break;
                case Constants::VERTICAL_NAME:
                    return new VerticalSorter($array);
                    break;
                case Constants::SNAKE_NAME:
                    return new SnakeSorter($array);
                    break;
                case Constants::DIAGONAL_NAME:
                    return new DiagonalSorter($array);
                    break;
                case Constants::HORISONTAL_VERTICAL_NAME:
                    return new HorisontalVerticalSorter($array);
                    break;
                case Constants::SNAIL_NAME:
                    return new SnailSorter($array);
                    break;
                default:
                    throw new FactoryException("The <b>$sorter</b> is out of stock. Pls, choose another sorter!");
                    break;
            }
        }
        catch (FactoryException $factoryException) {
            $factoryException->showException();
        }
    }
}
