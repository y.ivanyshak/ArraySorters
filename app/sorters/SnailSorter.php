<?php

namespace load\app\sorters;

use load\app\Constants;

final class SnailSorter extends AbstractSorter
{
    protected $name = Constants::SNAIL_NAME;

    final public function sort()
    {
        $sortedarray = [];
        $right = true;
        $left = false;

        $x = 0;
        $y = 0;
        $rowstart = 0;
        $columnstart = 0;
        $rowend = $this->rows - 1;
        $columnend = $this->columns - 1;

        for ($i = 0; $i < $this->rows; $i++) {
            for ($j = 0; $j < $this->columns; $j++) {
                $sortedarray[$x][$y] = $this->array[$i][$j];

                if ($right) {
                    if ($y == $columnend) {
                        if ($x == $rowend) {
                            $right = false;
                            $left = true;
                            $rowstart++;
                            $rowend--;
                        } else {
                            $x++;
                        }
                    } else {
                        $y++;
                    }
                }

                if ($left) {
                    if ($y == $columnstart) {
                        if ($x == $rowstart) {
                            $left = false;
                            $right = true;
                            $columnstart++;
                            $columnend--;
                            $y++;
                        } else {
                            $x--;
                        }
                    } else {
                        $y--;
                    }
                }
            }
        }

        return $sortedarray;
    }
}
