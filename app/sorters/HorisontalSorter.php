<?php

namespace load\app\sorters;

use load\app\Constants;

final class HorisontalSorter extends AbstractSorter
{
    protected $name = Constants::HORISONTAL_NAME;

    final public function sort()
    {
        $count = 0;
        $sortedarray = [];

        $mergedarray = SorterTools::mergeArray($this->array);
        sort($mergedarray);

        for ($i = 0; $i < $this->rows; $i++) {
            for ($j = 0; $j < $this->columns; $j++) {
                $sortedarray[$i][$j] = $mergedarray[$count];
                $count++;
            }
        }

        return $sortedarray;
    }
}
