<?php

namespace load\app\sorters;

use load\app\Constants;

final class VerticalSorter extends AbstractSorter
{
    protected $name = Constants::VERTICAL_NAME;

    final public function sort()
    {
        $count = 0;
        $sortedarray = [];

        foreach ($this->array as $row) {
            for ($i = 0; $i < $this->columns; $i++) {
                $sortedarray[$i][$count] = $row[$i];
            }

            $count++;
        }

        return $sortedarray;
    }
}
