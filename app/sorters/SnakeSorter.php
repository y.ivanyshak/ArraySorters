<?php

namespace load\app\sorters;

use load\app\Constants;

final class SnakeSorter extends AbstractSorter
{
    protected $name = Constants::SNAKE_NAME;

    final public function sort()
    {
        $sortedarray = [];
        $right = true;
        $left = false;

        $x = 0;
        $y = 0;

        for ($i = 0; $i < $this->rows; $i++) {
            for ($j = 0; $j < $this->columns; $j++) {
                $sortedarray[$x][$y] = $this->array[$i][$j];

                if ($right) {
                    if ($y == ($this->columns - 1)) {
                        $right = false;
                        $left = true;
                        $x++;
                    } else {
                        $y++;
                    }
                } elseif ($left) {
                    if ($y == 0) {
                        $left = false;
                        $right = true;
                        $x++;
                    } else {
                        $y--;
                    }

                }
            }
        }

        return $sortedarray;
    }
}
