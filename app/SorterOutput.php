<?php

namespace load\app;

use load\app\sorters\AbstractSorter;
use load\app\exceptions\ModeException;

final class SorterOutput
{
    final public function __invoke(AbstractSorter $sorter, $mode, $filename = "output.txt")
    {
        $bmode = substr_count($mode, 'b');
        $cmode = substr_count($mode, 'c');
        $fmode = substr_count($mode, 'f');

        try {
            if ($bmode and $cmode) {
                throw new ModeException("Trying to use both 'b' (Browser) and 'c'(CLI) modes in one sorter->output() call");
            }

            if (($bmode + $cmode + $fmode) == 0) {
                throw new ModeException("Undefined flags. Use 'c' - CLI or 'b' - Browser and/or 'f' - File as a 2nd argument");
            }

            $intext = $sorter->getName().' in:';
            $outtext = $sorter->getName().' out:';

            if ($bmode or $cmode) {
                if ($bmode) {
                    $nextrow = '<br/>';
                    $printmode = 'b';
                } elseif ($cmode) {
                    $nextrow = PHP_EOL;
                    $printmode = 'c';
                }

                echo $intext, $nextrow;
                $sorter->printArrayBC($sorter->array, $printmode);
                echo $nextrow, $outtext, $nextrow;
                $sorter->printArrayBC($sorter->sort(), $printmode);
                echo str_repeat('-', 20), $nextrow;
            }

            if ($fmode) {
                $handle = fopen("..\\storage\\$filename", "a");

                fwrite($handle, "$intext\n");
                $sorter->printArrayToFile($sorter->array, $filename);    # 2nd arg - $filename = "output.txt"
                fwrite($handle, "\n$outtext\n");
                $sorter->printArrayToFile($sorter->sort(), $filename);   # 2nd arg - $filename = "output.txt"
                fwrite($handle, str_repeat('-', 20)."\n");

                fclose($handle);
            }
        }
        catch (ModeException $modeException) {
            $modeException->showException();
        }
    }
}
