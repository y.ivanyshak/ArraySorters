<?php

namespace load\app\exceptions;

final class ArrayException extends AbstractException
{
    public function __construct($message = "Inputed value isn't array", $code = 401, AbstractException $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
