<?php

namespace load\app\exceptions;

final class ModeException extends AbstractException
{
    public function __construct($message = "You are choosing a wrong mode", $code = 400, AbstractException $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
