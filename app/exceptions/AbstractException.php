<?php

namespace load\app\exceptions;

use Exception;

abstract class AbstractException extends Exception
{
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    final public function showException()
    {
        echo 'Exception - '.$this->getCode().":";
        echo " '".$this->getMessage()."',";
        echo ' thrown in: <b>'.$this->getFile().'</b>';
        echo ' on line - <b>'.$this->getLine().'</b>'.'<br/>';
        #exit();
    }
}
