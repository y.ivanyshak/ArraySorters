<?php

namespace load\app\exceptions;

final class FactoryException extends AbstractException
{
    public function __construct($message = "The good is out of stock", $code = 404, AbstractException $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
