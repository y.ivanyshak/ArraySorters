<?php

namespace load\app;

use load\app\exceptions\FactoryException;
use load\app\patterns\SingletonTrait;

final class ArrayFactory
{
    use SingletonTrait;

    private static $array1 = [
        [4, 3, 5, 6, 7],
        [4, 3, 5, 6, 7],
        [4, 3, 5, 6, 7],
        [4, 3, 5, 6, 7],
        [4, 3, 5, 6, 7]
    ];

    private static $array2 = [
        [1, 1, 1, 1, 1],
        [2, 2, 2, 2, 2],
        [3, 3, 3, 3, 3],
        [4, 4, 4, 4, 4],
        [5, 5, 5, 5, 5]
    ];

    private static $array3 = [
        [1, 2, 3, 4, 5],
        [6, 7, 8, 9, 10],
        [11, 12, 13, 14, 15],
        [16, 17, 18, 19, 20],
        [21, 22, 23, 24, 25]
    ];

    final public static function getArray($num)
    {
        try {
            switch ($num) {
                case 1:
                    return self::$array1;
                    break;
                case 2:
                    return self::$array2;
                    break;
                case 3:
                    return self::$array3;
                    break;
                default:
                    throw new FactoryException("The <b>case - $num</b> is out of stock. Pls, choose another case number!");
                    break;
            }
        }
        catch (FactoryException $factoryException) {
            $factoryException->showException();
        }
    }
}
