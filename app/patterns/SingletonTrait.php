<?php

namespace load\app\patterns;

trait SingletonTrait
{
    private static $instance = null;

    private function __construct() {}

    private function __clone() {}

    private function __wakeup() {}

    final public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new self();
        }

        return static::$instance;
    }
}
