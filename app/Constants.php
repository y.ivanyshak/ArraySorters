<?php

namespace load\app;

final class Constants
{
    const HORISONTAL_NAME = "Horisontal";
    const VERTICAL_NAME = "Vertical";
    const SNAKE_NAME = "Snake";
    const DIAGONAL_NAME = "Diagonal";
    const HORISONTAL_VERTICAL_NAME = "Horisontal-Vertical";
    const SNAIL_NAME = "Snail";
}