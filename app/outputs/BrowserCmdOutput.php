<?php

namespace load\app\outputs;

trait BrowserCmdOutput
{
    final public function printArrayBC($array, $mode)
    {
        $maxlen = OutputTools::getMaxLen($array);
        $columns = count($array[0]);

        if (substr_count($mode, 'b')) {
            $spmode = '&nbsp;';
            $coef = 2;
            $nextrow = '<br/>';
        } elseif (substr_count($mode, 'c')) {
            $spmode = ' ';
            $coef = 1;
            $nextrow = PHP_EOL;
        }

        foreach ($array as $row) {
            for ($i = 0; $i < $columns; $i++) {
                $numlen = strlen(strval($row[$i]));
                $space = ($maxlen - $numlen) + 1;
                echo str_repeat($spmode, $coef*$space), $row[$i];
            }

            echo $nextrow;
        }
    }
}
