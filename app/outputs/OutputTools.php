<?php

namespace load\app\outputs;

final class OutputTools
{
    final public static function getMaxLen($array)
    {
        $maxlen = 0;
        $columns = count($array[0]);

        foreach ($array as $row) {
            for ($i = 0; $i < $columns; $i++) {
                $numlen = strlen(strval($row[$i]));
                if ($numlen > $maxlen) {
                    $maxlen = $numlen;
                }
            }
        }

        return $maxlen;
    }
}
