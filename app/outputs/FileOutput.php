<?php

namespace load\app\outputs;

trait FileOutput
{
    public function printArrayToFile($array, $filename = "output.txt")
    {
        $maxlen = OutputTools::getMaxLen($array);
        $columns = count($array[0]);

        $handle = fopen("..\\storage\\$filename", "a");

        foreach ($array as $row) {
            for ($i = 0; $i < $columns; $i++) {
                $numlen = strlen(strval($row[$i]));
                $space = ($maxlen - $numlen) + 1;
                fwrite($handle, str_repeat(' ', $space));
                fwrite($handle, $row[$i]);
            }

            fwrite($handle, "\n");
        }

        fclose($handle);
    }
}
