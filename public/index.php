<?php

require __DIR__."/../vendor/autoload.php";

use load\app\ArrayFactory;
use load\app\sorters\SorterFactory;
use load\app\SorterOutput;

// Arrays
$array1 = ArrayFactory::getArray(1);
$array2 = ArrayFactory::getArray(2);
$array3 = ArrayFactory::getArray(3);

// Sorters
$factory = SorterFactory::getInstance();
$diagonal = $factory->getSorter('Diagonal', $array2);
$horisontal = $factory->getSorter('Horisontal', $array1);
$horivert = $factory->getSorter('Horisontal-Vertical', $array2);
$snail = $factory->getSorter('Snail', $array2);
$snake = $factory->getSorter('Snake', $array3);
$vertical = $factory->getSorter('Vertical', $array2);

// Outputs - 1st arg(SorterInterface $sorter), 2nd arg ('c' - CLI or 'b' - Browser and/or 'f' - File), 3rd arg ($filename='output.txt')
$output = new SorterOutput();
$output($horisontal, 'bf');
$output($vertical, 'bf');
$output($snake, 'bf');
$output($diagonal, 'bf');
$output($horivert, 'bf');
$output($snail, 'bf');
